<h1>GitHub Octicons</h1>
<p>Ever wanted to use <a href="https://github.com/blog/1106-say-hello-to-octicons">Octicons</a> on your site?  Now you can!</p>

<p>Just add this to your page:</p>

<pre>
&lt;link rel="stylesheet" href="http://rummik.github.io/octicon/octicons.min.css"&gt;
</pre>

<p>See <a href="https://github.com/styleguide/css/7.0">https://github.com/styleguide/css/7.0</a> for class names.</p>

<small>Disclaimer: Octicons are copyrighted by GitHub, Inc.  I am not affiliated in any way with GitHub.  I just think the icons are fancy.</small>
